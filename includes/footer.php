
<p id="footerText">Office Health</p>

<div class="somet">
        <a href="https://www.facebook.com/" class="fa fa-facebook"></a>
        <a href="https://www.instagram.com/" class="fa fa-instagram"></a>
        <a href="https://www.twitter.com/" class="fa fa-twitter"></a>
        <a href="https://www.youtube.com/" class="fa fa-youtube"></a>
        <p>#officehealth</p>
</div>
<p id="info"><i class="footerIcons fas fa-envelope-square"></i> officehealth@email.com <i class="footerIcons fas fa-phone-square-alt"></i> 012 3456789 <i class="footerIcons fas fa-map-marker-alt"></i> Kivakatu 24 A, 01234 Espoo</p>
<hr>
<p id="copyright"><i class="far fa-copyright"></i> Copyright 2021 </p>
