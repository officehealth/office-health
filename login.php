<?php
  //sisällytetään header
  include_once('includes/header.php');
?>
<div class="imagediv col-12"></div>
  <div class="loginText">
    <h1>Sovellus, jonka avulla voit seurata terveyttäsi työpäivän ohella!</h1>
  </div>
  <div class="loginInformation col-12">
   <?php
    //Kirjautumisformi
    include_once('forms/loginForm.php')
   ?>
  </div>
</div>
<footer>
<?php
    //Footer
    include("includes/footer.php");
?>
</footer>
</body>
</html>
