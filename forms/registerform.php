<?php
include_once('config/config.php');

 ?>
<div class="registerForm">
<fieldset><legend>Rekisteröityminen</legend>
<form method="post" id="register">
<p>Käyttäjänimi *
  <br /> <input type="text" name="givenUsername" placeholder="Käyttäjänimi vähintään 3 kirjainta" maxlength="40" pattern="[a-zA-Z]{3,}"/>
  </p><p>
  Ikä *
  <br />  <input type="number" name="givenAge" placeholder="Kelpaava ikä" min="0" maxlength="2"/>
  </p><p>
    Pituus (cm) *
    <br />  <input type="number" name="givenHeight" placeholder="Kelpaava pituus (CM)" min="0" maxlength="3"/> <!-- Laitetaan nyt senttimetreinä, mutta vaihdetaan mahdollisesti kilogrammoiksi BMI -->
  </p><p>
    Paino (kg) *
    <br />  <input type="number" name="givenWeight" placeholder="Kelpaava paino (KG)" min="0" maxlength="3"/>
  </p><p>
    Sukupuoli *
    <br />
    <select class="sukupuoli" name="givenGender" form="register">
      <option value="mies">Mies</option>
      <option value="nainen">Nainen</option>
      <option value="muu">Muu</option>
    </select>
  </p><p>
  Salasana *
  <br />  <input type="password" name="givenPassword" placeholder="Salasana vähintään 8 merkkiä" maxlength="40"/>
  </p><p>
  Vahvista salasana *
  <br />  <input type="password" name="givenPasswordVerify" placeholder="Salasana uudelleen" maxlength="40"/>
  </p>
  <p>* = Pakollinen kenttä</p>
  <p>
  <br />  <input type="submit" name="submitUser" value="Tallenna"/>
          <input type="submit"  value="Takaisin" name="submitBack"/>
  </p>
</form>
</fieldset>
</div>

  <?php
  //Lomakkeen submit painettu?
  if(isset($_POST['submitUser'])){
    //Tarkistetaan syötteet myös palvelimella
    if(strlen($_POST['givenUsername'])<3){
     $_SESSION['swarningInput']="Ei sallittu salasana (väh 3 kirjainta)";
   }else if(strlen($_POST['givenAge'])<1){
     $_SESSION['swarningInput']="Ikää ei asetettu";
   }else if(strlen($_POST['givenHeight'])>4){
       $_SESSION['swarningInput']="Ei sallittu pituus (väh 3 merkkiä)";
     }else if(strlen($_POST['givenWeight'])>4){
         $_SESSION['swarningInput']="Ei sallittu paino (väh 3 merkkiä)";
    }else if(strlen($_POST['givenPassword'])<8){
    $_SESSION['swarningInput']="Ei sallittu salasana (väh 8 merkkiä)";
    }else if($_POST['givenPassword'] != $_POST['givenPasswordVerify']){
    $_SESSION['swarningInput']="Salasanat eivät täsmää";
    }else{
    unset($_SESSION['swarningInput']);
    //1. Tiedot sessioon
    $_SESSION['suserName']=$_POST['givenUsername'];
    $_SESSION['sloggedIn']="yes";
    //2. Tiedot kantaan - kesken

    $data['name'] = $_POST['givenUsername'];
    $added='#â‚¬%&&/'; //suolataan annettu salasana
    $data['pwd'] = password_hash($_POST['givenPassword'].$added, PASSWORD_BCRYPT);
    $data['age'] = $_POST['givenAge'];
    $data['height'] = $_POST['givenHeight'];
    $data['weight'] = $_POST['givenWeight'];
    $data['gender'] = $_POST['givenGender'];
    try {
    //***Username ei saa olla käytetty aiemmin
    $sql = "SELECT COUNT(*) FROM officehealth_user where userName  =  " . "'".$_POST['givenUsername']."'" ;
    var_dump($sql);
    $kysely=$DBH->prepare($sql);
    $kysely->execute();
    $tulos=$kysely->fetch();
    if($tulos[0] == 0){ //username ei ole käytössä
     $STH = $DBH->prepare("INSERT INTO officehealth_user (userName, userPass, userAge, userHeight, userWeight, userGender) VALUES (:name, :pwd, :age, :height, :weight, :gender);");
     $STH->execute($data);
     header("Location: index.php"); //Palataan pääsivulle kirjautuneena
    }else{
      $_SESSION['swarningInput']="Käyttäjänimi on jo käytössä";
    }
    } catch(PDOException $e) {
    file_put_contents('log/DBErrors.txt', 'signInUser.php: '.$e->getMessage()."\n", FILE_APPEND);
    $_SESSION['swarningInput'] = 'Database problem';

  }


    //Testataan pääsivulle paluu

    //Palataan pääsivulle jos tallennus onnistui -kesken
      header("Location: index.php");
   }
  }
  ?>
  <?php
  //Luovutetaanko ja palataan takaisin pääsivulle alkutilanteeseen
  if(isset($_POST['submitBack'])){
    session_unset();
    session_destroy();
    header("Location: login.php");
  }
  ?>

  <?php
    //Näytetäänkö lomakesyötteen aiheuttama varoitus?
  if(isset($_SESSION['swarningInput'])){
    echo("<p class=\"warning\"Virhe: ". $_SESSION['swarningInput']."</p>");

  }
  ?>
