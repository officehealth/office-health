<?php
include_once("config/config.php");
?>

<!-- Loginin lomake -->
<div class="loginForm">
<fieldset><legend>Kirjautuminen</legend>
<form method="post" id="login">
<p>Käyttäjänimi *
  <br /> <input type="text" name="givenUsername" placeholder="Anna käyttäjänimi" maxlength="40"/>
  </p>
  <p>
  Salasana *
  <br />  <input type="password" name="givenPassword" placeholder="Anna salasana" maxlength="40"/>
  </p>
  <p>
  <br />  <input type="submit" name="logIn" value="Kirjaudu"/>
  </p>
  <p>* = Pakollinen kenttä</p>
  <p>
  <br />  <a href="register.php">Luo tili</a>
  </p>
</form>
</fieldset>
</div>

<?php
//Lomakkeen submit painettu?
  if(isset($_POST['logIn'])){
 //Kirjautumisen validointi
 if(empty($_POST['givenUsername']) || empty($_POST['givenPassword'])) {
   echo "Käyttäjänimi tai salasana on väärin";
   }else{
     unset($_SESSION['swarningInput']);
      try {
       //Tiedot kannasta, hakuehto
       $data['name'] = $_POST['givenUsername'];
       //$data['password'] = $_POST['givenPassword'];
       $STH = $DBH->prepare("SELECT userName, userPass FROM officehealth_user WHERE userName = :name");
       $STH->execute($data);
       $STH->setFetchMode(PDO::FETCH_OBJ);
       $tulosOlio=$STH->fetch();
       //Lomakkeelle annettu salasana + suola
       $givenPasswordAdded = $_POST['givenPassword'].$added; //$added löytyy cconfig.php
          //Löytyikö käyttäjä kannasta?
          if($tulosOlio!=NULL){
            //Käyttäjä löytyi
            //Jos käyttäjänimi ja salasana oikein kirjaudutaan sisälle
            if(password_verify($givenPasswordAdded,$tulosOlio->userPass)){
                $_SESSION['sloggedIn']="yes";
                $_SESSION['suserName']=$tulosOlio->userName;
                $_SESSION['suserPass']=$tulosOlio->userPass;
                header("Location: index.php"); //Palataan pääsivulle kirjautuneena
            }else{
              //Virheilmoitus väärästä salasanasta tai käyttäjänimestä
              $_SESSION['swarningInput']="Väärä salasana tai käyttäjänimi";

            }
        }else{
          //Virheilmoitus väärästä käyttäjänimestä tai salasanasta
          $_SESSION['swarningInput']="Väärä salasana tai käyttäjänimi";

        }
       } catch(PDOException $e) {
          file_put_contents('log/DBErrors.txt', 'register.php: '.$e->getMessage()."\n", FILE_APPEND);
          $_SESSION['swarningInput'] = 'Database problem';
      }
    }
  }


  //Näytetäänkö lomakesyötteen aiheuttama varoitus?
if(isset($_SESSION['swarningInput'])){
  echo("<p class=\"warning\">Virhe: ". $_SESSION['swarningInput']."</p>");
}
?>
