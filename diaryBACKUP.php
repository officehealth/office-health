<?php
session_start();
if(!isset($_SESSION['sloggedIn'])){
  header('Location: login.php');
}
include_once("config/config.php");
include_once("config/https.php");

?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <title>Päiväkirja </title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="styles/main.css">
  <link rel="stylesheet" href="styles/footerStyles.css">
  <script src="https://kit.fontawesome.com/3a9c0f3274.js" crossorigin="anonymous"></script>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
  <div id="wrapper">
  <nav class="col-12">
  <div class="navbarlogo">
        <h1>OFFICE HEALTH</h1>
      </div>
      <div class="links">
        <a href="index.php">Etusivu</a>
        <a href="diary.php" class="bolded">Päiväkirja</a>
        <a href="#">Kalenteri</a>
        <a href="profile.php">Profiili</a>
        <a href="logout.php">Kirjaudu ulos</a>
      </div>
    </nav>

<!-- <div class="chart col-12">

include("includes/charts.php");
 -->
<div class="main col-9">
<?php
include("includes/charts.php");
$data1['name'] = $_SESSION['suserName'];
$sql1 = "SELECT userID FROM officehealth_user where userName = :name";
$kysely=$DBH->prepare($sql1);
$kysely->execute($data1);
$tulos=$kysely->fetch();
$currentUserID=$tulos['userID'];

$data2["name"] = $currentUserID;
$sql2 = "SELECT * FROM officehealth_user_entry WHERE officehealth_user_entry.userPersonID = :name";
$kysely2=$DBH->prepare($sql2);
$kysely2->execute($data2);
while ($row = $kysely2->fetch(PDO::FETCH_OBJ)) {
  echo ("<div class=\"entryId col-12\">");
  echo ("<div title='Päivämäärä' class=\"time col-3 text-center\"><i class=\"big-icon far fa-clock\"></i> <br> $row->entryDate</div>");
  echo ("<div title='Olotila' class=\"condition col-3 text-center\"><i class=\"big-icon far fa-smile\"></i> <br> $row->condition2</div>");
  echo ("<div title='Unen määrä' class=\"sleep col-3 text-center\"><i class=\"big-icon fas fa-bed\"></i> <br> $row->sleep tuntia</div>");
  echo ("<div title='Aktiviteetti' class=\"activity col-3 text-center\"><i class=\"big-icon fas fa-running\"></i> <br> $row->activity</div>");
  echo ("</div>");
  if ($row == NULL){
    break;
  }
}
?>
</div>
<div class="sidebar col-3">
      <label>Järjestä:</label>
      <br>
      <select name="dropdown" class="dropdown">
        <option value="newest">Uusimmasta vanhimpaan</option>
        <option value="oldest">Vanhimmasta uusimpaan</option>
      </select>
      <br>
      <a href="forms/entryForm.php">
      <button  class="button1">+ Uusi merkintä</button>
      </a>
</div>

<footer>
<?php
    //Footer
    include("includes/footer.php");
?>
</footer>
</body>
</html>
