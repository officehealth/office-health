<?php
  //sisällytetään header
  include_once('includes/header.php')
?>
<div class="imagediv col-12"></div>
  <div class="loginText">
    <h1>Sovellus, jonka avulla voit seurata terveyttäsi työpäivän ohella!</h1>
  </div>
  <div class="information col-12">
    <?php
     //Rekisteröitymisformi
     include_once('forms/registerform.php')
    ?>
  </div>
</div>
<footer>
<?php
    //Footer
    include("includes/footer.php");
?>
</footer>
</body>
</html>
