<?php
// Start session, and check if user is logged in before letting him into the page
session_start();
if(!isset($_SESSION['sloggedIn'])){
  header('Location: login.php');
}
// Load in needed config files for database and HTTPS protocol
include_once("config/config.php");
include_once("config/https.php");

?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <title>Päiväkirja </title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="styles/main.css">
  <link rel="stylesheet" href="styles/diaryStyles.css">
  <link rel="stylesheet" href="styles/footerStyles.css">
  <script src="https://kit.fontawesome.com/3a9c0f3274.js" crossorigin="anonymous"></script>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
  <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
  <script src="https://cdn.amcharts.com/lib/4/themes/material.js"></script>
  <script src="https://cdn.amcharts.com/lib/4/themes/dataviz.js"></script>
</head>
<body onload='showFeedback()'>
  <div id="wrapper">
  <nav class="col-12">
      <div class="navbarlogo">
        <a href="index.php">
        <img class="logo" alt="logo" src="images/logo3.2.svg">
        </a>
      </div>
      <div id="myLinks" class="links">
        <a href="index.php">Etusivu</a>
        <a href="diary.php" class="bolded">Päiväkirja</a>
        <a href="calendar.php">Kalenteri</a>
        <a href="profile.php">Profiili</a>
        <a href="logout.php">Kirjaudu ulos</a>
      </div>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
    </nav>
    <script>
    /* Function to show and hide the contents inside the navbar when they click on the hamburger icon */
    function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
    </script>

<div class="main-body col-12">
<div class="diaryText col-9">
<p> Tällä Päiväkirja -sivulla pääset luomaan uuden päiväkirjamerkinnän sekä näkemään tehdyt merkintäsi. </p>
<p> Sovellus laskee merkintöjesi perusteella valmiutesi (readiness-arvo) ja näyttää sen ensimmäisessä graafissa sekä päiväkirjamerkinnöissäsi toiseksi viimeisenä arvona. </p>
<p> Readiness-arvo on luku välillä 0-100 ja se muodostuu unen, aktiivisuuden sekä sykkeen käyttäytymisen pohjalta. Mitä suurempi readiness-arvo on, sitä paremmin elimistösi on palautunut. </p>
<p> Toisessa graafissa sekä päiväkirjamerkinnöissä viimeisenä arvona näkyy päivän keskimääräinen sykevälivaihtelu eli HRV arvosi (millisekunteina) kuukauden ajalta. </p>
</div>
<div class="main col-9">
  <h1>Readiness-arvo</h1>
  <div class="chartdiv"></div>
  <h1>HRV-arvot</h1>
  <div class="chartdiv2"></div>
<?php

// Get all the diary entries from the specific user and show them as a list on the page
$data1['name'] = $_SESSION['suserName'];
$sql1 = "SELECT userID FROM officehealth_user where userName = :name";
$kysely=$DBH->prepare($sql1);
$kysely->execute($data1);
$tulos=$kysely->fetch();
$currentUserID=$tulos['userID'];
// Check if the "Order" button is clicked
if (isset($_POST['order'])) {
// Check if the dropdown menu is set to "oldest", and organise all entries by oldest first
  if ($_POST['dropdown'] == "oldest") {
    $data3["name"] = $currentUserID;
    $sql3= "SELECT * FROM officehealth_user_entry WHERE officehealth_user_entry.userPersonID = :name
    ORDER BY entryID ASC";
    $kysely3=$DBH->prepare($sql3);
    $kysely3->execute($data3);
    $i = 0;
    while ($row = $kysely3->fetch(PDO::FETCH_OBJ)) {
      echo ("<div class=\"entryId col-12\">");
      echo ("<div title='Päivämäärä' class=\"time col-2 text-center\"><i class=\"big-icon far fa-clock\"></i> <br> $row->entryDate</div>");
      echo ("<div title='Olotila' class=\"condition col-2 text-center\"><i class=\"big-icon far fa-smile\"></i> <br> $row->condition2</div>");
      echo ("<div title='Unen määrä' class=\"sleep col-2 text-center\"><i class=\"big-icon fas fa-bed\"></i> <br> $row->sleep tuntia</div>");
      echo ("<div title='Aktiviteetti' class=\"activity col-2 text-center\"><i class=\"big-icon fas fa-running\"></i> <br> $row->activity</div>");
      echo ("<div title='Readiness-arvo' class=\"readinessC col-2 text-center\"><i class=\"big-icon fas fa-wave-square\"></i> <br><div id=\"readiness$i\">0</div></div>");
      echo ("<div title='HRV-arvo' class=\"activity col-2 text-center\"><i class=\"big-icon fas fa-heartbeat\"></i> <br><div id=\"hrv$i\">0</div></div>");
      echo ("<div id='1' class='Palaute col-12' style='display:none;text-align:center'></div>");
      echo ("</div>");
    $i++;
    if ($row == NULL){
    break;
    }
  }}
  // elseif($_POST['dropdown'] == "newest") {
  // Organise the entries by newest first
    else{
    $data4["name"] = $currentUserID;
    $sql4= "SELECT * FROM officehealth_user_entry WHERE officehealth_user_entry.userPersonID = :name
    ORDER BY entryID DESC";
    $kysely4=$DBH->prepare($sql4);
    $kysely4->execute($data4);
    $i = 0;
    while ($row = $kysely4->fetch(PDO::FETCH_OBJ)) {
      echo ("<div class=\"entryId col-12\">");
      echo ("<div title='Päivämäärä' class=\"time col-2 text-center\"><i class=\"big-icon far fa-clock\"></i> <br> $row->entryDate</div>");
      echo ("<div title='Olotila' class=\"condition col-2 text-center\"><i class=\"big-icon far fa-smile\"></i> <br> $row->condition2</div>");
      echo ("<div title='Unen määrä' class=\"sleep col-2 text-center\"><i class=\"big-icon fas fa-bed\"></i> <br> $row->sleep tuntia</div>");
      echo ("<div title='Aktiviteetti' class=\"activity col-2 text-center\"><i class=\"big-icon fas fa-running\"></i> <br> $row->activity</div>");
      echo ("<div title='Readiness-arvo' class=\"readinessC col-2 text-center\"><i class=\"big-icon fas fa-wave-square\"></i> <br><div id=\"readiness$i\">0</div></div>");
      echo ("<div title='HRV-arvo' class=\"activity col-2 text-center\"><i class=\"big-icon fas fa-heartbeat\"></i> <br><div id=\"hrv$i\">0</div></div>");
      echo ("<div id='1' class='Palaute col-12' style='display:none;text-align:center'></div>");
      echo ("</div>");
    $i++;
    if ($row == NULL){
    break;
    }
   }}
}
// Show the default view of newest first.
else{
$data2["name"] = $currentUserID;
$sql2 = "SELECT * FROM officehealth_user_entry WHERE officehealth_user_entry.userPersonID = :name
ORDER BY entryID DESC";
$kysely2=$DBH->prepare($sql2);
$kysely2->execute($data2);
$i = 0;
while ($row = $kysely2->fetch(PDO::FETCH_OBJ)) {
  echo ("<div class=\"entryId col-12\">");
  echo ("<div title='Päivämäärä' class=\"time col-2 text-center\"><i class=\"big-icon far fa-clock\"></i> <br> $row->entryDate</div>");
  echo ("<div title='Olotila' class=\"condition col-2 text-center\"><i class=\"big-icon far fa-smile\"></i> <br> $row->condition2</div>");
  echo ("<div title='Unen määrä' class=\"sleep col-2 text-center\"><i class=\"big-icon fas fa-bed\"></i> <br> $row->sleep tuntia</div>");
  echo ("<div title='Aktiviteetti' class=\"activity col-2 text-center\"><i class=\"big-icon fas fa-running\"></i> <br> $row->activity</div>");
  echo ("<div title='Readiness-arvo' class=\"readinessC col-2 text-center\"><i class=\"big-icon fas fa-wave-square\"></i> <br><div id=\"readiness$i\">0</div></div>");
  echo ("<div title='HRV-arvo' class=\"activity col-2 text-center\"><i class=\"big-icon fas fa-heartbeat\"></i> <br><div id=\"hrv$i\">0</div></div>");
  echo ("<div id='1' class='Palaute col-12' style='display:none;text-align:center'></div>");
  echo ("</div>");
  $i++;
  if ($row == NULL){
    break;
  }
}
}
?>
</div>
<script>
/* Code for HRV Graph (second one on the page)*/
fetch('https://users.metropolia.fi/~janmikab/Metropoliakevat/office-health/api/hrv2.php')
  .then((response) => {
	return response.json();
  })
  .then((data) => {

    // Data chart with our Data
    am4core.useTheme(am4themes_dataviz);
    am4core.useTheme(am4themes_material);
    // Themes end

    // Create chart
    var chart = am4core.create("chartdiv2", am4charts.XYChart);
    chart.paddingRight = 20;

    chart.data = data;
    chart.dateFormatter.inputDateFormat = "MM-dd";

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    // Set settings
    dateAxis.title.text = "Päivä";
    dateAxis.renderer.minGridDistance = 100;
    // Setting the accuracy of the chart (Day, month, year)
    dateAxis.baseInterval = {
      "timeUnit": "MM-dd",
      "count": 1
    }

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    // Value axis label name
    valueAxis.title.text = "HRV(MS)";

    var series = chart.series.push(new am4charts.StepLineSeries());
    series.dataFields.dateX = "day";
    series.dataFields.valueY = "value";
    series.tooltipText = "{valueY.value}";
    series.strokeWidth = 3;

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.xAxis = dateAxis;
    chart.cursor.fullWidthLineX = true;
    chart.cursor.lineX.strokeWidth = 0;
    chart.cursor.lineX.fill = chart.colors.getIndex(2);
    chart.cursor.lineX.fillOpacity = 0.1;

    chart.scrollbarX = new am4core.Scrollbar();

    // For loop to automatically place HRV-value
    for (var ii = 0; ii < 100; ii++) {
      document.getElementById("hrv"+ii).innerHTML=data[ii].value;
      console.log("Numero:" + ii);
    }

});


/* Fetchataan APISTA tiedot ja näytetään ne sivulla ensimmäisessä graafissa Readiness arvoina*/
fetch('https://users.metropolia.fi/~janmikab/Metropoliakevat/office-health/api/hrv.php')
  .then((response) => {
	return response.json();
  })
  .then((data) => {

    // Data chart with our Data
    am4core.useTheme(am4themes_material);
    // Themes end

    // Create chart
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;

    chart.data = data;
    chart.dateFormatter.inputDateFormat = "MM-dd";

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 100;
    // Set settings
    dateAxis.title.text = "Päivä";
    // Setting the accuracy of the chart (Day, month, year)
    dateAxis.baseInterval = {timeUnit:"MM-dd", count:1};

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    //Axis label name
    valueAxis.title.text = "Readiness-arvo";

    var series = chart.series.push(new am4charts.StepLineSeries());
    series.dataFields.dateX = "day";
    series.dataFields.valueY = "value";
    series.tooltipText = "{valueY.value}";
    series.strokeWidth = 3;

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.xAxis = dateAxis;
    chart.cursor.fullWidthLineX = true;
    chart.cursor.lineX.strokeWidth = 0;
    chart.cursor.lineX.fill = chart.colors.getIndex(2);
    chart.cursor.lineX.fillOpacity = 0.1;

    chart.scrollbarX = new am4core.Scrollbar();

    // For loop to automatically place HRV
    for (var ii = 0; ii < 100; ii++) {
      document.getElementById("readiness"+ii).innerHTML=data[ii].value;
      console.log("Numero:" + ii);
    }

});


// HARDCODED function to Give an "analysis" to the user. PROOF OF CONCEPT!!!
function showFeedback() {

  document.getElementsByClassName('Palaute')[0].innerHTML = "Tämä on hyvä Readiness-arvo!";
  document.getElementsByClassName('Palaute')[0].style.cssText = "display: block; color: green; text-align:center";
  document.getElementsByClassName('readinessC')[0].style.cssText = "display: block; color: green";
  document.getElementsByClassName('Palaute')[1].innerHTML = "Tämä on hyvä Readiness-arvo!";
  document.getElementsByClassName('Palaute')[1].style.cssText = "display: block; color: green; text-align:center";
  document.getElementsByClassName('readinessC')[1].style.cssText = "display: block; color: green";
  document.getElementsByClassName('Palaute')[3].innerHTML = "Tämä on matala Readiness-arvo!";
  document.getElementsByClassName('Palaute')[3].style.cssText = "display: block; color: red; text-align:center";
  document.getElementsByClassName('readinessC')[3].style.cssText = "display: block; color: red";
  document.getElementsByClassName('Palaute')[4].innerHTML = "Tämä on hyvä Readiness-arvo!";
  document.getElementsByClassName('Palaute')[4].style.cssText = "display: block; color: green; text-align:center";
  document.getElementsByClassName('readinessC')[4].style.cssText = "display: block; color: green";
  document.getElementsByClassName('Palaute')[6].innerHTML = "Tämä on matala Readiness-arvo!";
  document.getElementsByClassName('Palaute')[6].style.cssText = "display: block; color: red; text-align:center";
  document.getElementsByClassName('readinessC')[6].style.cssText = "display: block; color: red";
  document.getElementsByClassName('Palaute')[8].innerHTML = "Tämä on hyvä Readiness-arvo!";
  document.getElementsByClassName('Palaute')[8].style.cssText = "display: block; color: green; text-align:center";
  document.getElementsByClassName('readinessC')[8].style.cssText = "display: block; color: green";


  /*for (var d = 0; d < 100; d++) {
    if (a < 100) {
      console.log("Arvo on:" + a);
      var y = document.getElementsByClassName('Palaute')[d];
      y.innerHTML = "Tämä on hyvä arvo!";
      y.style.display = "block";
    }
  }*/
  /*if (z < 100) {
  y.innerHTML = "Tämä on hyvä arvo!";
  y.style.display = "block";
}*/

/*if (y.style.display === "block") {
y.style.display = "none";
} else {
y.style.display = "block";
}*/
}


</script>
<div class="sidebar col-3">
<form method="post">
      <label>Järjestä:</label>
      <input class="button2 fa" type="submit" name="order" value="&#xf2f1"/>
      <br>
      <select name="dropdown" class="dropdown">
        <option name="newest" value="newest">Uusin ensin</option>
        <option name="oldest" value="oldest">Vanhin ensin</option>
      </select>
</form>

<br>
<a href="forms/entryForm.php">
<button  class="button1">+ Uusi merkintä</button>
</a>
</div>
</div>

<footer>
<?php
    //Footer
    include("includes/footer.php");
?>
</footer>
</body>
</html>
