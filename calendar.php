<?php
session_start();
if(!isset($_SESSION['sloggedIn'])){
  header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <title>Office health Kalenteri</title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="styles/main.css">
  <link rel="stylesheet" href="styles/footerStyles.css">
  <link rel="stylesheet" href="styles/indexStyles.css">
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href='fullcalendar/main.css' rel='stylesheet' />
  <script src="https://kit.fontawesome.com/3a9c0f3274.js" crossorigin="anonymous"></script>
    <script src='fullcalendar/main.js'></script>
    <script>

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          initialView: 'dayGridMonth',
          selectable: true
        });
        calendar.render();
      });

    </script>
</head>
<body>
  <div id="wrapper">
    <nav class="col-12">
      <div class="navbarlogo">
        <a href="index.php">
        <img class="logo" alt="logo" src="images/logo3.2.svg">
        </a>
      </div>
      <div id="myLinks" class="links">
        <a href="index.php">Etusivu</a>
        <a href="diary.php">Päiväkirja</a>
        <a href="calendar.php" class="bolded">Kalenteri</a>
        <a href="profile.php">Profiili</a>
        <a href="logout.php">Kirjaudu ulos</a>
      </div>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
    </nav>
    <script>
    function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
    </script>
    <div id='calendar' style="margin: auto;margin-top: 1%;margin-bottom: 1%"></div>
<footer>
<?php
    //Footer
    include("includes/footer.php");
?>
</footer>
</body>
</html>
