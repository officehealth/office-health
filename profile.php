<?php
/* Start the session and check if the user is logged in before being let into the site */
session_start();
if(!isset($_SESSION['sloggedIn'])){
  header('Location: login.php');
}
include_once("config/config.php");
?>
<!DOCTYPE html>
<html lang="fi-FI">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Office health profiili</title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="styles/main.css">
  <link rel="stylesheet" href="styles/profileStyles.css">
  <link rel="stylesheet" href="styles/footerStyles.css">
  <script src="https://kit.fontawesome.com/3a9c0f3274.js" crossorigin="anonymous"></script>
  <meta charset="UTF-8"/>
</head>
<body>
  <div id="wrapper">
    <nav class="col-12">
      <div class="navbarlogo">
        <a href="index.php">
        <img class="logo" alt="logo" src="images/logo3.2.svg">
        </a>
      </div>
      <div id="myLinks" class="links">
        <a href="index.php">Etusivu</a>
        <a href="diary.php">Päiväkirja</a>
        <a href="calendar.php">Kalenteri</a>
        <a href="profile.php" class="bolded">Profiili</a>
        <a href="logout.php">Kirjaudu ulos</a>
      </div>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
    </nav>
    <script>
    /* Function to make the navbar hamburger icon clicable and show/hide the links inside it */
    function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }
    </script>

    <div class="container col-12">
    <div class="userInfo col-6">
    <h1>Profiilitietosi tällä hetkellä</h1>
    <?php
    //käyttäjä haetaan, jotta sen profiilitiedot voidaan hakea ja näyttää
    $name1 = $_SESSION['suserName'];
    $sql="SELECT userName, userAge, userHeight, userWeight, userGender
    FROM officehealth_user  WHERE userName = " . "'".$_SESSION['suserName']."'" ;
    $kysely=$DBH->prepare($sql);
    $kysely->execute();
    //käyttäjän tämän hetkiset profiilitiedot haetaan ja näytetään taulukossa
	  echo("<table>

		<tr>
			<th>Käyttäjänimi</th>
			<th>Ikä</th>
			<th>Pituus</th>
			<th>Paino</th>
      <th>Sukupuoli</th></tr>");

		while	($row=$kysely->fetch()){
				echo("<tr><td>".$row["userName"]."</td>
				<td>".$row["userAge"]."</td>
				<td>".$row["userHeight"]."</td>
				<td>".$row["userWeight"]."</td>
        <td>".$row["userGender"]."</td></tr>");
		}

    echo("</table>");
    ?>
    <div class="accDeletion">
      <?php
      //käyttäjän poistaminen
      include_once('forms/deleteAcc.php');
      ?>
    </div>
    </div>
    <div class="information col-6">
      <?php
      //käyttäjän profiilitietojen muokkaamisformi
      include_once('forms/profileForm.php');
      ?>
    </div>

    </div>
  </div>
  <footer>
    <?php
    //Footer
    include("includes/footer.php");
    ?>
  </footer>
</body>
</html>
